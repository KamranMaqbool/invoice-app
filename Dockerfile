# pull the official base image
FROM python:3.8-slim-buster

# set work directory
WORKDIR /app
# set environment variables
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN pip install --upgrade pip
COPY ./requirements.txt /app

RUN pip install -r requirements.txt
COPY . /app

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0:8000"]