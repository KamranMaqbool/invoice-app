import json

from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Invoice


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "email", "username", "first_name", "last_name")


class InvoiceSerializer(serializers.ModelSerializer):
    is_credit_for = UserSerializer(read_only=True)

    class Meta:
        model = Invoice
        fields = (
            "id",
            "is_credit_for",
            "items",
            "received_amount",
            "receiver_name",
            "status",
            "title",
            "total_amount",
            "receiver_phone_no",
            "receiver_email",
            "created_at",
        )

    def create(self, validated_data):
        invoice_items = validated_data.pop("items")
        invoice_json_items = json.dumps(invoice_items)
        user = self.context["request"].user
        invoice = Invoice.objects.create(
            is_credit_for=user, items=invoice_json_items, **validated_data
        )
        return invoice
