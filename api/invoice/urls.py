from django.urls import path

from .views import (InvoiceCreateListView, InvoiceDestroyUpdateView,
                    generate_pdf, viewPdf)

urlpatterns = [
    path("", InvoiceCreateListView.as_view()),
    path("item/<int:pk>/", InvoiceDestroyUpdateView.as_view()),
    path("download-pdf/<int:id>", generate_pdf, name="generate-pdf"),
    path("view-pdf/", viewPdf, name="view-pdf"),
]
