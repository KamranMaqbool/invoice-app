from django.contrib.auth.models import User
from django.db import models

from .managers import InvoiceCustomQuerySet

# Create your models here.


class Invoice(models.Model):
    PAID = "paid"
    UN_PAID = "unpaid"

    CHOICES_TYPE = (
        (PAID, "Paid"),
        (UN_PAID, "Un Paid"),
    )

    title = models.CharField(max_length=20, blank=True, null=True)
    total_amount = models.DecimalField(
        max_digits=6, decimal_places=2, blank=True, null=True, default=0
    )
    received_amount = models.DecimalField(
        max_digits=6, decimal_places=2, blank=True, null=True, default=0
    )
    status = models.CharField(max_length=20, choices=CHOICES_TYPE)
    is_credit_for = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="invoice", blank=True, null=True
    )
    receiver_name = models.CharField(max_length=50, null=True, blank=True)
    receiver_phone_no = models.CharField(max_length=255, null=True, blank=True)
    receiver_email = models.EmailField(max_length=255)
    items = models.JSONField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = InvoiceCustomQuerySet()  # The default manager.

    def __str___(self):
        return self.title

    @property
    def amount_status(self):
        amount = self.received_amount - self.total_amount
        return amount
