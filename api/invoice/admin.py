from django.contrib import admin

from .models import Invoice

# Register your models here.


@admin.register(Invoice)
class Invoice(admin.ModelAdmin):
    pass
