import json

from django.http import HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics
from xhtml2pdf import pisa

from .models import Invoice
from .serializers import InvoiceSerializer

# Create your views here.


class InvoiceCreateListView(generics.ListCreateAPIView):
    """
    Create & List of invoice.
    """

    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ["status"]
    search_fields = ["title"]

    def get_queryset(self):
        return (
            super(InvoiceCreateListView, self)
            .get_queryset()
            .filter(is_credit_for_id=self.request.user.id)
        )


class InvoiceDestroyUpdateView(generics.RetrieveUpdateDestroyAPIView):
    """
    Reterive, update, destroy invoice.
    """

    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer


def generate_pdf(request, id=None):
    """
    Generate pdf.
    """
    invoice = Invoice.objects.get(id=id)
    template_path = "invoice/invoice-pdf.html"

    response = HttpResponse(content_type="application/pdf")
    response["Content-Disposition"] = 'attachment; filename="Invoice.pdf"'

    invoice_items = json.loads(invoice.items)
    html = render_to_string(
        template_path,
        {"invoice": invoice, "invoice_items": invoice_items, "user": request.user},
    )

    pisa_status = pisa.CreatePDF(html, dest=response)
    # if error then show some funy view
    if pisa_status.err:
        return HttpResponse("We had some errors <pre>" + html + "</pre>")
    return response


def viewPdf(request):
    """
    Invoice pdf view.
    """
    invoice = Invoice.objects.get(id=7)
    invoice_items = json.loads(invoice.items)
    context = {
        "invoice_from": request.user.username,
        "invoice": invoice,
        "invoice_items": invoice_items,
    }
    return render(request, "invoice/invoice-pdf.html", context)
