from django.contrib import auth
from django.contrib.auth.models import User
from django.http import JsonResponse
from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken

from .serializers import RegisterSerializer, UserSerializer

# Create your views here.


class RegisterUserView(generics.CreateAPIView):
    """
    Register User with profile.
    """

    queryset = User.objects.all()
    serializer_class = RegisterSerializer


class UpdateUserProfile(APIView):
    """
    Update User Profile.
    """

    def patch(self, request, format=None):
        user = request.user
        serializer = UserSerializer(user, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(generics.GenericAPIView):
    """
    Login User.
    """

    permission_classes = [AllowAny]

    def post(self, request):
        data = request.data
        email = data.get("email", "")
        password = data.get("password", "")
        user = auth.authenticate(username=email, password=password)
        if user:
            refresh = RefreshToken.for_user(user)

            serializer = RegisterSerializer(user)

            data = {
                "message": "Login successfully",
                "user": serializer.data,
                "access": str(refresh.access_token),
                "refresh": str(refresh),
                "status": status.HTTP_200_OK,
            }

            return Response(data, status=status.HTTP_200_OK)

        return Response(
            {"detail": "Invalid credentials"}, status=status.HTTP_401_UNAUTHORIZED
        )


class LoadUser(APIView):
    """
    Get current user.
    """

    def get(self, request):
        try:
            user = request.user
            user = UserSerializer(user)
            return Response({"user": user.data})
        except User.DoesNotExist:
            return JsonResponse(
                {"error": "user not found"}, status=status.HTTP_404_NOT_FOUND
            )
