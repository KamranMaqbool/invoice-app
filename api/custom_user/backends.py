import jwt
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import authentication, exceptions


class JWTAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        auth_data = authentication.get_authorization_header(request)
        if not auth_data:
            return None

        prefix, token = auth_data.decode("utf-8").split(" ")

        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
            user = User.objects.get(email=payload["email"])
            return (user, token)

        # except jwt.DecodeError as identifier:
        except jwt.DecodeError:
            raise exceptions.AuthenticationFailed("Your token is invalid,please login")
        # except jwt.ExpiredSignatureError as identifier:
        except jwt.ExpiredSignatureError:
            raise exceptions.AuthenticationFailed("Your token is expired,please login")

        return super().authenticate(request)
