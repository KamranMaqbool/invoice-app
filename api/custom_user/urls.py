from django.urls import path

from .views import LoadUser, LoginView, RegisterUserView, UpdateUserProfile

urlpatterns = [
    path("update-profile/", UpdateUserProfile.as_view()),
    path("register/", RegisterUserView.as_view()),
    path("login/", LoginView.as_view()),
    path("load-user/", LoadUser.as_view()),
]
