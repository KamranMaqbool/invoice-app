from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import Profile


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ["address", "phone_number", "user", "id"]


class RegisterSerializer(serializers.ModelSerializer):

    email = serializers.EmailField(
        max_length=255,
        min_length=5,
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())],
    )
    password = serializers.CharField(
        max_length=55, min_length=6, required=True, write_only=True
    )
    confirm_password = serializers.CharField(
        max_length=55, min_length=6, required=True, write_only=True
    )

    class Meta:
        model = User
        fields = ("id", "email", "password", "confirm_password")
        extra_kwargs = {}

    def validate(self, data):
        if data["password"] != data["confirm_password"]:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."}
            )
        return super().validate(data)

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data["email"],
            email=validated_data["email"],
            password=validated_data["password"],
        )
        Profile.objects.create(user=user)
        return user


class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer()

    class Meta:
        model = User
        exclude = ("password",)

    def update(self, instance, validated_data):
        profile_data = validated_data.pop("profile")
        profile_instance = instance.profile

        # update user data
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.last_name = validated_data.get("last_name", instance.last_name)
        instance.save()
        # update user profile data
        profile_instance.address = profile_data.get("address", profile_instance.address)
        profile_instance.phone_number = profile_data.get(
            "phone_number", profile_instance.phone_number
        )
        profile_instance.save()

        return instance
