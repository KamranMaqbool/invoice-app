from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Profile(models.Model):
    address = models.CharField(max_length=100, blank=True, null=True)
    phone_number = models.CharField(max_length=255, blank=True, null=True)
    user = models.OneToOneField(
        User, related_name="profile", on_delete=models.CASCADE, editable=False
    )
